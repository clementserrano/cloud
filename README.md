# Cloud 
## Groupe F
- JOLIVET Nathan
- SERRANO Clément
- TAGUEJOU Christian

## Command list
`npm start` Start the server with pm2, in background

`npm stop` Stop the server with pm2

`npm run log` Print the last 15 lines of server logs, and stream current logs

`npm run monit` Display the monitoring menu from pm2

`npm run dev` Start the server in development mode, restarting after any change
    
`npm run lint` Start quality code check

`npm run npm-dview` Check libraries updates

`npm run kill` Kill the application process