const express = require('express');
const router = express.Router();
const User = require('./User');
const dto = require('./dto.js');
const fs = require('fs');

router.get('/user', (req, res) => {
    const pageNum = req.query.page == null ? 0 : parseInt(req.query.page);
    const pageSize = 100;

    User.find().skip(pageNum * pageSize).limit(pageSize).then(users => {
        res.json(dto.usersDTO(users));
    }).catch(err => {
        console.log('Erreur GET /user/ : ' + err);
        res.status(500).send(err);
    });
});

router.get('/user/age', (req, res) => {
    const pageNum = req.query.page != undefined ? parseInt(req.query.page) : 0;
    const greaterThan = parseInt(req.query.gt);
    const equal = parseInt(req.query.eq);
    const pageSize = 100;
    const birthDayParam = new Date();

    if (req.query.gt != undefined) {
        if (greaterThan < 0) return res.status(400).send();
        birthDayParam.setFullYear(birthDayParam.getFullYear() - greaterThan);

        User.find({birthDay: {$lte: birthDayParam}}).skip(pageNum * pageSize).limit(pageSize).then(users => {
            res.json(dto.usersDTO(users));
        }).catch(err => {
            console.log('Erreur GET /user/age gt : ' + err);
            res.status(500).send(err);
        });
    }
    else if (req.query.eq != undefined) {
        if (equal < 0) return res.status(400).send();
        birthDayParam.setFullYear(birthDayParam.getFullYear() - equal);
        const birthDayParamEnd = new Date();
        birthDayParamEnd.setFullYear(birthDayParamEnd.getFullYear() - equal - 1);

        User.find({
            birthday: {
                $gte: birthDayParamEnd,
                $lte: birthDayParam
            }
        }).skip(pageNum * pageSize).limit(pageSize).then(users => {
            res.json(dto.usersDTO(users));
        }).catch(err => {
            console.log('Erreur GET /user/age eq : ' + err);
            res.status(500).send(err);
        });
    }
});

router.get('/user/search', (req, res) => {
    const pageNum = req.query.page != undefined ? parseInt(req.query.page) : 0;
    const pageSize = 100;
    const name = req.query.term;
    if (name != undefined) {
        User.find({$or: [{firstName: name}, {lastName: name}]}).skip(pageNum * pageSize).limit(pageSize).then(users => {
            res.json(dto.usersDTO(users));
        }).catch(err => {
            console.log('Erreur GET /user/search : ' + err);
            res.status(500).send(err);
        });
    } else {
        res.status(400).send();
    }
});

router.get('/user/nearest', (req, res) => {
    const pageNum = req.query.page != undefined ? parseInt(req.query.page) : 0;
    const pageSize = 10;
    const lat = req.query.lat;
    const lon = req.query.lon;

    if (lat != undefined && lon != undefined) {
        const locationParam = {
            location: {
                $near: {
                    $geometry: {
                        type: 'Point',
                        coordinates: [lon, lat]
                    }
                }
            }
        };

        User.find(locationParam).skip(pageNum * pageSize).limit(pageSize).then(users => {
            res.json(dto.usersDTO(users));
        }).catch(err => {
            console.log('Erreur GET /user/nearest : ' + err);
            res.status(500).send(err);
        });
    } else {
        res.status(400).send();
    }
});

router.put('/user', (req, res) => {
    const data = req.body;
    User.deleteMany({}).then(function () {
        User.insertMany(dto.users(data)).then(response => {
            res.status(201).json(response);
        }).catch(err => {
            console.log('Erreur PUT /user/ (insertMany) : ' + err);
            res.status(500).send(err);
        });
    }).catch(err => {
        console.log('Erreur PUT /user/ (deleteMany) : ' + err);
        res.status(500).send(err);
    });
});

router.delete('/user', (req, res) => {
    User.deleteMany({}).then(function () {
        res.json({});
    }).catch(err => {
        console.log('Erreur DELETE /user/ : ' + err);
        res.status(500).send(err);
    });
});

router.post('/user', (req, res) => {
    const userPost = req.body;
    const user = new User(dto.user(userPost));
    user.save().then((user) => {
        res.status(201).send(dto.userDTO(user));
    }).catch((err) => {
        console.log('Erreur POST /user/ : ' + err);
        res.status(500).send(err);
    });
});

// To fill the database with 100 users
router.get('/user/fill', (req, res) => {
    const data = JSON.parse(fs.readFileSync('src/data.json', 'utf8'));
    User.insertMany(dto.users(data)).then(response => {
        res.status(201).json(response);
    }).catch(err => {
        console.log('Erreur PUT /user/fill (insertMany) : ' + err);
        res.status(500).send(err);
    });
});

module.exports = router;