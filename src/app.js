const mongoose = require('mongoose');
const express = require('express');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const enforce = require('express-sslify');

// Connexion BD
let mongoUrl;

const env = process.env.NODE_ENV || 'development';

if (env == 'production' || env == 'test') {
    const user = 'ur3qvb1ckfbaldy';
    const password = 'Jf4zWs7JrIRB7vIo0CvX';
    const host = 'bisjejbc3uircci-mongodb.services.clever-cloud.com';
    const portBD = 27017;
    const database = 'bisjejbc3uircci';
    mongoUrl = 'mongodb://' + user + ':' + password + '@' + host + ':' + portBD + '/' + database;
} else { // Development environment
    mongoUrl = 'mongodb://localhost:27017/cloud';
}

mongoose.connect(mongoUrl, {useNewUrlParser: true})
    .then(() => console.log('... connection BD succesful'))
    .catch((err) => console.error(err));

// To disable warnings
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

// Configuration Serveur
const app = express();
const port = 8080;

app.use(helmet());
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}));
app.use(bodyParser.json({limit: '10mb', extended: true}));

// Redirect to https
if (env == 'production') {
    app.use(enforce.HTTPS({trustProtoHeader: true}));
    console.log('... https');
}

const server = app.listen(port, () => {
    console.log('Server started on port ' + port + ' ... ');
    console.log('... ' + env + ' environment');
});

// Routes
const routesGlobal = require('./routesGlobal');
const routesSpecific = require('./routesSpecific');

app.use('/', routesGlobal);
app.use('/', routesSpecific);

module.exports = server;