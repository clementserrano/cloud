const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const User = require('./User');
const dto = require('./dto.js');

router.get('/user/:id', (req, res) => {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(404).send();
    User.findOne({'_id': req.params.id}).then(user => {
        if (user == {} || user == null) return res.status(404).json({});
        res.json(dto.userDTO(user));
    }).catch(err => {
        console.log('Erreur GET /user/' + req.params.id + ' : ' + err);
        res.status(500).send(err);
    });
});

router.put('/user/:id', (req, res) => {
    const user = dto.updateUser(req.body);
    User.findOneAndUpdate(req.params.id, user).then((response) => {
        res.status(200).json(response);
    }).catch((err) => {
        console.log('Erreur PUT /user/' + req.params.id + ' : ' + err);
        res.status(500).send(err);
    });
});

router.delete('/user/:id', (req, res) => {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(500).send();
    User.deleteOne({'_id': req.params.id}).then(function () {
        res.json(req.params.id + ' deleted !');
    }).catch(err => {
        console.log('Erreur DELETE /user/' + req.params.id + ' : ' + err);
        return res.status(500).send(err);
    });
});

module.exports = router;