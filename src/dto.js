const dateFormat = require('dateformat');

const dto = {
    usersDTO: function (users) {
        if (users.length == 0) return [];

        return users.map(function (user) {
            return dto.userDTO(user);
        });
    },
    userDTO: function (user) {
        return {
            id: user._id,
            firstName: user.firstName,
            lastName: user.lastName,
            birthDay: dateFormat(user.birthDay, 'mm/dd/yyyy'),
            position: user.position
        };
    },
    user: function (userDTO) {
        return {
            firstName: userDTO.firstName,
            lastName: userDTO.lastName,
            birthDay: userDTO.birthDay,
            position: userDTO.position,
            location: {
                type: 'Point',
                coordinates: [userDTO.position.lon, userDTO.position.lat]
            }
        };
    },
    users: function (usersDTO) {
        if (usersDTO.length == 0) return [];

        return usersDTO.map(function (user) {
            return dto.user(user);
        });
    },
    updateUser: function (userDTO) {
        if (userDTO.position != undefined) userDTO.location.coordinates = [userDTO.position.lon, userDTO.position.lat];
        return userDTO;
    }
};

module.exports = dto;