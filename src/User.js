const mongoose = require('mongoose');
const joi = require('joi');
const joigoose = require('joigoose')(mongoose);

let collection = 'users';
if (process.env.NODE_ENV == 'test') collection = 'usersTest';

const joiSchema = joi.object().keys({
    firstName: joi.string().required(),
    lastName: joi.string().required(),
    birthDay: joi.date().required(),
    position: {
        lat: joi.number().required(),
        lon: joi.number().required()
    },
    // Field for $geoNear queries
    location: {
        type: joi.string().required().required(),
        coordinates: joi.array().items(joi.number()).required()
    }
});

const userSchema = new mongoose.Schema(joigoose.convert(joiSchema));
userSchema.index({location: '2dsphere'});

module.exports = mongoose.model(collection, userSchema);